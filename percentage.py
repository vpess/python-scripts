#Recebe dois valores, e retorna o valor do aumento entre eles, em porcentagem.

V1 = float(input('Insira o valor do primeiro mês: '))
V2 = float(input('Insira o valor do segundo mês: '))

perc =  ((V2-V1) / V1) * 100

print ('O valor do aumento: {:.1f}%'.format(perc))
